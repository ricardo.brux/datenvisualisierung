/*
 * DataSet class:
 * Main model class, used to store timestamps and DataChannels.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef DATASET_H
#define DATASET_H

#include "datachannel.h"

#include <QObject>

class DataSet
{
public:
    static DataSet* getInstance(); //static, singleton class - getInstance() used to get static pointer

    int getTimestamp(int t);
    void addTimestamp(int t);
    DataChannel* getDatachannel(int c);			//functions used to manipulate and gather data stored in DataSet class
    void addDatachannel();
    void clearData();

    std::vector<int> getTimestamps() const;				//timestamps getter & setter.
    void setTimestamps(const std::vector<int> &value);

    std::vector<DataChannel *> getChannels() const;		//DataChannels getter & setter.
    void setChannels(const std::vector<DataChannel *> &value);

private:
    static DataSet theDS; //static singleton object
    DataSet(); //private constructor to stop dynamic generation of DataSet objects

    std::vector<int> timestamps;			//Data
    std::vector<DataChannel*> channels;
};

#endif // DATASET_H
