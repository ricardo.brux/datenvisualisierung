#include "datachannel.h"

DataChannel::DataChannel(){}

void DataChannel::addRecord(int record)
{
    data.push_back(record);
}

void DataChannel::replaceData(std::vector<int> newData)
{
    data.clear();
    data = newData;
}

std::vector<int> DataChannel::getData()
{
    return data;
}

int DataChannel::getFirstValue()
{
    return data.front();
}

int DataChannel::getLastValue()
{
    return data.back();
}

int DataChannel::getValue(int i)
{
    return data[i];
}

int DataChannel::getChannelID() const
{
    return channelID;
}

void DataChannel::setChannelID(int value)
{
    channelID = value;
}

int DataChannel::getConverter() const
{
    return converter;
}

void DataChannel::setConverter(int value)
{
    converter = value;
}

int DataChannel::getCalibratedHorizontalMinIndex() const
{
    return calibratedHorizontalMinIndex;
}

void DataChannel::setCalibratedHorizontalMinIndex(int value)
{
    calibratedHorizontalMinIndex = value;
}

int DataChannel::getCalibratedHorizontalMaxIndex() const
{
    return calibratedHorizontalMaxIndex;
}

void DataChannel::setCalibratedHorizontalMaxIndex(int value)
{
    calibratedHorizontalMaxIndex = value;
}
