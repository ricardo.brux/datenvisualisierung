#include "dataset.h"

DataSet DataSet::theDS;

DataSet::DataSet(){}

DataSet *DataSet::getInstance()
{
    return &theDS;
}

int DataSet::getTimestamp(int t)
{
    return timestamps[t];
}

void DataSet::addTimestamp(int t)
{
    timestamps.push_back(t);
}

DataChannel *DataSet::getDatachannel(int c)
{
    return channels[c];
}

void DataSet::addDatachannel()
{
    channels.push_back(new DataChannel);
}

void DataSet::clearData()
{
    channels.clear();		//clear methods used to reset application.
    timestamps.clear();
}

std::vector<int> DataSet::getTimestamps() const
{
    return timestamps;
}

void DataSet::setTimestamps(const std::vector<int> &value)
{
    timestamps = value;
}

std::vector<DataChannel *> DataSet::getChannels() const
{
    return channels;
}

void DataSet::setChannels(const std::vector<DataChannel *> &value)
{
    channels = value;
}
