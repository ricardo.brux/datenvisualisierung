/*
 * DataChannel class:
 * Stores a channels values and UI manipulation data.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef DATACHANNEL_H
#define DATACHANNEL_H

#include <QObject>

class DataChannel
{
public:
    DataChannel();
    void addRecord(int record);
    void replaceData(std::vector<int> newData);
    std::vector<int> getData();						//Functions used to gather and manipulate channel values.
    int getFirstValue();
    int getLastValue();
    int getValue(int i);

    int getChannelID() const;
    void setChannelID(int value);

    int getConverter() const;
    void setConverter(int value);

    int getCalibratedHorizontalMinIndex() const;
    void setCalibratedHorizontalMinIndex(int value);

    int getCalibratedHorizontalMaxIndex() const;
    void setCalibratedHorizontalMaxIndex(int value);

private:
    int channelID; //Unique channel ID
    int converter; // AD converter used by channel
    std::vector<int> data; //int values of channel

    int calibratedHorizontalMinIndex; //UI parameters. Used once calibration is complete and data is sent to view. Index of timestamp min timestamp value needed in selection.
    int calibratedHorizontalMaxIndex; //These integers are used to avoid having to construct a whole new timestamp vector once user selects a specific timespan to display.
};

#endif // DATACHANNEL_H
