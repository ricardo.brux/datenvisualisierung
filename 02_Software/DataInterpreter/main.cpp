/*
 *
 * -------------------------------------------------
 *
 *                  DataInterpreter
 *     Easily visualize and condition datasteams
 *
 *              ----------------------
 *
 *              Written by Brux Ricardo
 *               HES-SO Valais Wallis
 *
 * -------------------------------------------------
 *
 */


/*
 * Main method:
 * Initialize Factory and begin application.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#include "Factory/factory.h"
#include "Exception/exception.h"

#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    try
    {
        QApplication a(argc, argv); //QApplication initialization. Main reason behind limited Singleton-Pattern usage in project - QApplication needs to initialize before any QWidgets are initlilzed, and since QApplication happens at the start of the main method, it's not possible to have static QWidgets - They'd be initilized before the main method gets called.

        Factory* TheF = new Factory(); //non-static Factory initialization
        TheF->init();
        TheF->build();		//Factory methods
        //TheF->start();

        return a.exec();
    }
    catch(Exception &e)
    {
        std::cout << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}
