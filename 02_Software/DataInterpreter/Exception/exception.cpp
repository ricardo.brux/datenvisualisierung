#include "exception.h"

Exception::Exception(ERRORS errorId)
{
    switch (errorId) {
        case PARSINGERROR:
            message = "An error occured when trying to read serialport data.\nCheck output data structure.";
            break;
        case INVALIDTIMESTAMP:
            message = "An error occured when trying to process timestamp data.\nCheck ouput data structure and timestamp validity.";
            break;
        case VISUALIZATIONERROR:
            message = "An error occured when trying to visualize data.";
            break;
    }
}
