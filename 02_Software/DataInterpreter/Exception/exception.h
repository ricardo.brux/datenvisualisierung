/*
 * Exception class:
 * Holds types of exceptions and their corresponding messages.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <exception>
#include <QString>

class Exception : public std::exception
{
public:
    typedef enum ERRORS { //error types
        PARSINGERROR, INVALIDTIMESTAMP, VISUALIZATIONERROR
    } ERRORS;

    Exception(ERRORS errorId);
    const char* what() const noexcept override {return message.toLocal8Bit().data();}

private:
    QString message; //error message
};

#endif // EXCEPTION_H
