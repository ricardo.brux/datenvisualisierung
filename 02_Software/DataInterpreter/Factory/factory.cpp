#include "factory.h"

Factory::Factory(){}

void Factory::init()
{
	//non-singleton initialisation
    TheCW = new ConfigurationWindow();
    TheDW = new DataWindow();

	//non-static factory passed on as paramted. Classes save pointer and refer to it to call Factory.
    theCW()->init(this);
    theDW()->init(this);
    theIC()->init(this);
}

void Factory::build()
{
	//set InputController Observer
    theIC()->setObserver(theDW());
}

//no statemachines, thus start() function is empty.
void Factory::start(){}
