/*
 * Factory class:
 * Used to initialize and easily refer to other classes.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef FACTORY_H
#define FACTORY_H

#include "../Controller/inputcontroller.h"
#include "../View/datawindow.h"
#include "../View/configurationwindow.h"
#include "../Model/dataset.h"

class Factory
{
public:
    Factory();
    void init();
    void build();
    void start();

	//class references
    static InputController* theIC() {return InputController::getInstance();}
    static DataSet* theDS() {return DataSet::getInstance();}
    DataWindow* theDW() {return TheDW->getInstance();}
    ConfigurationWindow* theCW() {return TheCW->getInstance();}

private:
    DataWindow* TheDW;
    ConfigurationWindow* TheCW;
};

#endif // FACTORY_H
