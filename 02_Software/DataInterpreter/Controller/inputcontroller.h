/*
 * InputController class:
 * Controller used to parse data, carry out calculations, as well as bridge the gap between model and view.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef INPUTCONTROLLER_H
#define INPUTCONTROLLER_H

#include <QObject>
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QRegExp>
#include <QTimer>
#include <QFile>
#include <QMessageBox>
#include <ctime>
#include <QDir>
#include <QTextStream>

#include "../Model/datachannel.h"
#include "../Exception/exception.h"

#define MAX_VALUE 931 //max 10bit value representing 3.0V (nucleo sends data between 0.0 to 3.0 volt)

class Factory; //Factory class declaration to avoid recursive includes. Factory is not static and thus the ConfigurationWindow has to save the Factory's pointer -> Factory* TheF.

class InputController : public QObject
{
    Q_OBJECT
public:
    static InputController* getInstance(); //return static pointer
    void init(Factory* F); //save non-static factory pointer

    class ISensorObserver //observer interface - to be inherited by DataWindow
    {
    public:
        virtual void transmitChannels(std::vector<DataChannel*> channels) = 0;
        virtual void changeWindowSize(int w, int h) = 0;
        virtual void setChannelAmount(int n) = 0;
        virtual void setCalibration(std::vector<int> timestamps, int ticksPerSec) = 0;
        virtual void emitUpdateSignal() = 0;
        virtual void resetCalibration() = 0;
        virtual void resetUI() = 0;
    };

    void setObserver(ISensorObserver*);

private:
    static InputController theIC; //static singleton
    InputController(); //private constructor to stop dynamic generation of DataSet objects
    Factory* TheF; //non-static factory pointer
    QSerialPort* serialPort;
    bool serialPortOpen;
    ISensorObserver* obs;
    bool newConn; //true if port has just been opened
    int channelAmount;
    QTimer portCheck;

    int ticksPerSec; //conversion rate from microcontroller ticks to seconds
    QTimer calibrationTimer; //timer of calibration
    bool calibrated;
    int calibrationMS; //calibration timer lenght
    int elapsedTime; //time elapsed since start of port connection

    DataChannel* writeCalibratedChannelData(int c); //function to optimize channel construction under calibration
    void reset(); //reset all variables

    std::vector<double> horizontalMinScale;
    std::vector<double> horizontalMaxScale;

    std::vector<double> verticalMinScale;       //DataWindow UI element value vectors
    std::vector<double> verticalMaxScale;

    std::vector<QString> dimensions;

private slots:
    void onPortOpen();
    void onPortClose();
    void onSerialData();
    void calibrate();
    void portInvalid();
    void updateHorizontalScale(double, double, double, double, double, double, double, double);
    void updateVerticalScale(double, double, double, double, double, double, double, double);
    void updateDimensions(QString, QString, QString, QString);
    void createLog();
    void processCustomScript(QString, int, int, int); //receive script data

signals:
    void sendScriptDisplayData(int, QString, bool); //send off processed script data
};

#endif // INPUTCONTROLLER_H
