#include "inputcontroller.h"
#include "../Factory/factory.h"

InputController InputController::theIC;

InputController::InputController(){}

InputController *InputController::getInstance()
{
    return &theIC;
}

void InputController::init(Factory* F)
{
    TheF = F;

    serialPortOpen = false;

    connect(TheF->theCW()->getOpenPort(),SIGNAL(clicked()),this,SLOT(onPortOpen()));
    connect(TheF->theCW()->getClosePort(),SIGNAL(clicked()),this,SLOT(onPortClose()));
    connect(&calibrationTimer,SIGNAL(timeout()),this,SLOT(calibrate())); //calibrate once 5sec timer runs out
    connect(&portCheck,SIGNAL(timeout()),this,SLOT(portInvalid()));
    connect(TheF->theDW(),SIGNAL(updateHorizontalScale(double, double, double, double, double, double, double, double)),this,SLOT(updateHorizontalScale(double, double, double, double, double, double, double, double)));
    connect(TheF->theDW(),SIGNAL(updateVerticalScale(double, double, double, double, double, double, double, double)),this,SLOT(updateVerticalScale(double, double, double, double, double, double, double, double)));
    connect(TheF->theDW(),SIGNAL(updateDimensions(QString, QString, QString, QString)),this,SLOT(updateDimensions(QString, QString, QString, QString)));
    connect(TheF->theDW(),SIGNAL(sendCustomScript(QString, int, int, int)),this,SLOT(processCustomScript(QString, int, int, int)));
    connect(TheF->theCW(),SIGNAL(logData()),this,SLOT(createLog()));

    auto serialPortInfos = QSerialPortInfo::availablePorts();
    for (QSerialPortInfo &serialPortInfo : serialPortInfos) //add every available port to selector
    {
        TheF->theCW()->getPortList()->addItem(serialPortInfo.portName());
    }

    channelAmount = 0;
    newConn = true;
    calibrated = false;
    calibrationMS = 5000;
    calibrationTimer.setSingleShot(true);

    portCheck.setSingleShot(true);
    serialPort = new QSerialPort(this);
}

void InputController::onPortOpen()
{
    serialPort->setBaudRate(QSerialPort::Baud115200);
    serialPort->setDataBits(QSerialPort::Data8);
    serialPort->setPortName(TheF->theCW()->getPortList()->currentText());
    if (!serialPort->open(QIODevice::ReadOnly))
    {
        serialPortOpen = false;
        TheF->theCW()->displayInfo("Port not open.\nSelect different port.");
    }
    else
    {
        connect(serialPort,SIGNAL(readyRead()),this,SLOT(onSerialData()));
        serialPortOpen = true;
        TheF->theCW()->disableWidgets();
        portCheck.start(5000); //timer to check port validity
    }
}

void InputController::onPortClose()
{
    if (serialPortOpen)
    {
        emit createLog();
        reset();
        TheF->theCW()->displayInfo("Logfile saved in application folder /logging.");
    }
}

void InputController::onSerialData()
{
    QRegExp dataStructure("dataset\\{adc\\{timestamp\\{value=[0-9]+\\}(chan\\{conv=[0-9]+,id=[0-9]+,value=[0-9]+\\})+\\}\\}"); //expected data structure
    if ((serialPort->bytesAvailable()) >= 350)
    {
        QString s;
        s = serialPort->readLine();

        if(dataStructure.indexIn(s) != -1)
        {
            if(portCheck.isActive())
            {
                portCheck.stop();
                TheF->theCW()->togglePortState(true);
            }

            s = dataStructure.cap(0);
            if(newConn)
            {
                channelAmount = 0;

                QRegExp r1("chan");
                int pos = 0;
                int counter = 0;

                while ((pos = r1.indexIn(s, pos)) != -1 && counter < 4) //check channel amount
                {
                    channelAmount++;
                    TheF->theDS()->addDatachannel();
                    pos += r1.matchedLength();
                    counter++;
                }

                obs->setChannelAmount(channelAmount);

                QRegExp r2("conv=([0-9]+)|id=([0-9]+)");
                pos = 0;
                counter = 0;
                while ((pos = r2.indexIn(s, pos)) != -1) //configure channel metadata
                {
                    if (counter % 2)
                    {
                        if (counter == 1)
                        {
                            TheF->theDS()->getChannels()[0]->setChannelID(r2.cap(2).toInt());
                        }
                        else if (counter == 3)
                        {
                            TheF->theDS()->getChannels()[1]->setChannelID(r2.cap(2).toInt());
                        }
                        else if (counter == 5)
                        {
                            TheF->theDS()->getChannels()[2]->setChannelID(r2.cap(2).toInt());
                        }
                        else if (counter == 7)
                        {
                            TheF->theDS()->getChannels()[3]->setChannelID(r2.cap(2).toInt());
                        }
                    }
                    else
                    {
                        if (counter == 0)
                        {
                            TheF->theDS()->getChannels()[0]->setConverter(r2.cap(1).toInt());
                        }
                        else if (counter == 2)
                        {
                            TheF->theDS()->getChannels()[1]->setConverter(r2.cap(1).toInt());
                        }
                        else if (counter == 4)
                        {
                            TheF->theDS()->getChannels()[2]->setConverter(r2.cap(1).toInt());
                        }
                        else if (counter == 6)
                        {
                            TheF->theDS()->getChannels()[3]->setConverter(r2.cap(1).toInt());
                        }
                    }
                    pos += r2.matchedLength();
                    counter++;
                }

                if(obs)
                {
                    int y = 230*channelAmount;
                    obs->changeWindowSize(1350,y); //scale window size depending on channel amount
                }

                elapsedTime = 0;
                calibrationTimer.start(calibrationMS);
            }
            else
            {
                unsigned int parseCheck = TheF->theDS()->getTimestamps().size();

                QRegExp r3("value=([0-9]+)");
                int pos = 0;
                int counter = 0;
                while ((pos = r3.indexIn(s, pos)) != -1) //read channel data & timestamps
                {
                    if (counter == 0)
                    {
                        if(TheF->theDS()->getTimestamps().size() > 0)
                        {
                            if(r3.cap(1).toInt() <= TheF->theDS()->getTimestamps().back())
                            {
                                throw Exception(Exception::INVALIDTIMESTAMP); //throw exception timestamp is invalid
                            }
                        }

                        TheF->theDS()->addTimestamp(r3.cap(1).toInt());
                    }
                    else
                    {
                        int temp = r3.cap(1).toInt();
                        if(temp > MAX_VALUE && counter != 0)
                        {
                            temp = MAX_VALUE;
                        }

                        if (counter == 1)
                        {
                            TheF->theDS()->getChannels()[0]->addRecord(temp);
                        }
                        else if (counter == 2)
                        {
                            TheF->theDS()->getChannels()[1]->addRecord(temp);
                        }
                        else if (counter == 3)
                        {
                            TheF->theDS()->getChannels()[2]->addRecord(temp);
                        }
                        else if (counter == 4)
                        {
                            TheF->theDS()->getChannels()[3]->addRecord(temp);
                        }
                    }

                    pos += r3.matchedLength();
                    counter++;
                }

                if(TheF->theDS()->getTimestamps().size() == parseCheck)
                {
                    throw Exception(Exception::PARSINGERROR); //throw exception if no data was parsed
                }

                if(!calibrated && horizontalMinScale.size() < 1) //transmit regular channels if not calibrated
                {
                    obs->transmitChannels(TheF->theDS()->getChannels());
                }
                else //construct new channels if calibrated
                {
                    std::vector<DataChannel*> tempChannels;
                    obs->setCalibration(TheF->theDS()->getTimestamps(), ticksPerSec);

                    horizontalMinScale.resize(channelAmount);
                    horizontalMaxScale.resize(channelAmount);

                    for (int i = 0;i < channelAmount; i++)
                    {
                        tempChannels.push_back(writeCalibratedChannelData(i));
                    }

                    obs->transmitChannels(tempChannels);
                }
            }

            if(!newConn && !TheF->theDW()->isVisible())
            {
                TheF->theDW()->show();
                obs->emitUpdateSignal();
            }
            else
            {
                newConn = false;
            }

            TheF->theDW()->update();
        }
    }
}

DataChannel* InputController::writeCalibratedChannelData(int c)
{
    DataChannel* tempChannel = new DataChannel;
    bool channelFinished = false;
    bool channelWriting = false;

    //transfer channel metadata to new channel
    tempChannel->setChannelID(TheF->theDS()->getChannels()[c]->getChannelID());
    tempChannel->setConverter(TheF->theDS()->getChannels()[c]->getConverter());

    for(int counter = TheF->theDS()->getTimestamps().size() - 1; counter >= 0; counter--)
    {
        if(channelAmount > 0 && !channelFinished)
        {
            if(horizontalMinScale[c] < 0)
            {
                if(((((double(TheF->theDS()->getTimestamps()[counter])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) > ((double(double(TheF->theDS()->getTimestamps().back()-TheF->theDS()->getTimestamps()[0])/ticksPerSec)+horizontalMinScale[c])+horizontalMaxScale[c])) || horizontalMaxScale[c] == 0) && ((double(TheF->theDS()->getTimestamps()[counter]-TheF->theDS()->getTimestamps()[0])/ticksPerSec) < (double(double(TheF->theDS()->getTimestamps().back()-TheF->theDS()->getTimestamps()[0])/ticksPerSec)+horizontalMinScale[c]))) //calibrated timestamp rule of three: check whether channel value timestamps falls between user selected time points
                {
                    tempChannel->addRecord(TheF->theDS()->getChannels()[c]->getValue(counter)); //write channel value
                    if(!channelWriting)
                    {
                        tempChannel->setCalibratedHorizontalMaxIndex(counter); //update max horizontal timestamps index - used to avoid having to build a new timestamp vector
                    }
                    channelWriting = true;
                    if(counter == 0)
                    {
                        tempChannel->setCalibratedHorizontalMinIndex(1); //update min horizontal timestamps index - used to avoid having to build a new timestamp vector
                        tempChannel->setCalibratedHorizontalMaxIndex(tempChannel->getCalibratedHorizontalMaxIndex()+1); //update max horizontal timestamps index - used to avoid having to build a new timestamp vector
                        channelFinished = true;
                    }
                }
                else if(channelWriting)
                {
                    tempChannel->setCalibratedHorizontalMinIndex(counter); //update min horizontal timestamps index - used to avoid having to build a new timestamp vector
                    channelFinished = true;
                }
            }
            else if(horizontalMinScale[c] == 0)
            {
                if((((double(TheF->theDS()->getTimestamps()[counter])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) > (((double(TheF->theDS()->getTimestamps().back()) - double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec)+horizontalMaxScale[c])) || horizontalMaxScale[c] == 0) //calibrated timestamp rule of three: check whether channel value timestamps falls between user selected time points
                {
                    tempChannel->addRecord(TheF->theDS()->getChannels()[c]->getValue(counter)); //write channel value
                    if(!channelWriting)
                    {
                        tempChannel->setCalibratedHorizontalMaxIndex(counter); //update max horizontal timestamps index - used to avoid having to build a new timestamp vector
                    }
                    channelWriting = true;
                    if(counter == 0)
                    {
                        tempChannel->setCalibratedHorizontalMinIndex(1); //update min horizontal timestamps index - used to avoid having to build a new timestamp vector
                        tempChannel->setCalibratedHorizontalMaxIndex(tempChannel->getCalibratedHorizontalMaxIndex()+1); //update max horizontal timestamps index - used to avoid having to build a new timestamp vector
                        channelFinished = true;
                    }
                }
                else if(channelWriting)
                {
                    tempChannel->setCalibratedHorizontalMinIndex(counter); //update min horizontal timestamps index - used to avoid having to build a new timestamp vector
                    channelFinished = true;
                }
            }
            else //horizontalMinScale > 0
            {
                if(((((double(TheF->theDS()->getTimestamps()[counter])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) > (horizontalMinScale[c]+horizontalMaxScale[c])) || horizontalMaxScale[c] == 0) && (((double(TheF->theDS()->getTimestamps()[counter])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) < horizontalMinScale[c])) //calibrated timestamp rule of three: check whether channel value timestamps falls between user selected time points
                {
                    tempChannel->addRecord(TheF->theDS()->getChannels()[c]->getValue(counter)); //write channel value
                    if(!channelWriting)
                    {
                        tempChannel->setCalibratedHorizontalMaxIndex(counter); //update max horizontal timestamps index - used to avoid having to build a new timestamp vector
                    }
                    channelWriting = true;
                    if(counter == 0)
                    {
                        tempChannel->setCalibratedHorizontalMinIndex(1); //update min horizontal timestamps index - used to avoid having to build a new timestamp vector
                        tempChannel->setCalibratedHorizontalMaxIndex(tempChannel->getCalibratedHorizontalMaxIndex()+1); //update max horizontal timestamps index - used to avoid having to build a new timestamp vector
                        channelFinished = true;
                    }
                }
                else if(channelWriting)
                {
                    tempChannel->setCalibratedHorizontalMinIndex(counter); //update min horizontal timestamps index - used to avoid having to build a new timestamp vector
                    channelFinished = true;
                }
            }
        }
    }

    return tempChannel; //return constructed channel
}

void InputController::reset()
{
    serialPort->close();
    serialPortOpen = false;
    newConn = true;
    TheF->theDS()->clearData();
    TheF->theCW()->togglePortState(false);
    obs->resetCalibration();
    TheF->theDW()->close();                 //reset everything, ready up for new port
    calibrated = false;
    obs->resetUI();
    calibrationTimer.stop();
    horizontalMinScale.clear();
    horizontalMaxScale.clear();
    verticalMinScale.clear();
    verticalMaxScale.clear();
}

void InputController::calibrate()
{
    elapsedTime = elapsedTime + calibrationMS;
    ticksPerSec = (TheF->theDS()->getTimestamps().back() - TheF->theDS()->getTimestamps()[0])/(elapsedTime/1000); //caluculate tick - second conversion rate

    calibrationTimer.start(calibrationMS);
    calibrated = true;
}

void InputController::portInvalid()
{
    reset();
    TheF->theCW()->displayWarning("Invalid port.\nPlease select a different port.");
}

void InputController::updateHorizontalScale(double min1, double min2, double min3, double min4, double max1, double max2, double max3, double max4)
{
    if(channelAmount > 0)
    {
        if(horizontalMinScale.size() < 1)
        {
            horizontalMinScale.resize(1);
            horizontalMaxScale.resize(1);
        }
        horizontalMinScale[0] = min1;
        horizontalMaxScale[0] = max1;
    }
    if(channelAmount > 1)
    {
        if(horizontalMinScale.size() < 2)
        {
            horizontalMinScale.resize(2);
            horizontalMaxScale.resize(2);
        }
        horizontalMinScale[1] = min2;
        horizontalMaxScale[1] = max2;
    }
    if(channelAmount > 2)
    {
        if(horizontalMinScale.size() < 3)
        {
            horizontalMinScale.resize(3);
            horizontalMaxScale.resize(3);
        }
        horizontalMinScale[2] = min3;
        horizontalMaxScale[2] = max3;
    }
    if(channelAmount > 3)
    {
        if(horizontalMinScale.size() < 4)
        {
            horizontalMinScale.resize(4);
            horizontalMaxScale.resize(4);
        }
        horizontalMinScale[3] = min4;
        horizontalMaxScale[3] = max4;
    }
}

void InputController::updateVerticalScale(double min1, double min2, double min3, double min4, double max1, double max2, double max3, double max4)
{
    if(channelAmount > 0)
    {
        if(verticalMinScale.size() < 1)
        {
            verticalMinScale.resize(1);
            verticalMaxScale.resize(1);
        }
        verticalMinScale[0] = min1;
        verticalMaxScale[0] = max1;
    }
    if(channelAmount > 1)
    {
        if(verticalMinScale.size() < 2)
        {
            verticalMinScale.resize(2);
            verticalMaxScale.resize(2);
        }
        verticalMinScale[1] = min2;
        verticalMaxScale[1] = max2;
    }
    if(channelAmount > 2)
    {
        if(verticalMinScale.size() < 3)
        {
            verticalMinScale.resize(3);
            verticalMaxScale.resize(3);
        }
        verticalMinScale[2] = min3;
        verticalMaxScale[2] = max3;
    }
    if(channelAmount > 3)
    {
        if(verticalMinScale.size() < 4)
        {
            verticalMinScale.resize(4);
            verticalMaxScale.resize(4);
        }
        verticalMinScale[3] = min4;
        verticalMaxScale[3] = max4;
    }
}

void InputController::updateDimensions(QString d1, QString d2, QString d3, QString d4)
{
    if(channelAmount > 0)
    {
        if(dimensions.size() < 1)
        {
            dimensions.resize(1);
        }
        dimensions[0] = d1;
    }
    if(channelAmount > 1)
    {
        if(dimensions.size() < 2)
        {
            dimensions.resize(2);
        }
        dimensions[1] = d2;
    }
    if(channelAmount > 2)
    {
        if(dimensions.size() < 3)
        {
            dimensions.resize(3);
        }
        dimensions[2] = d3;
    }
    if(channelAmount > 3)
    {
        if(dimensions.size() < 4)
        {
            dimensions.resize(4);
        }
        dimensions[3] = d4;
    }
}

void InputController::createLog()
{
    time_t now = time(0);
    tm *ltm = localtime(&now); //get current time and date
    QString dt = ctime(&now);

    QString logname = "log_" + QString::number(ltm->tm_hour) + "-" + QString::number(ltm->tm_min) + "-" + QString::number(ltm->tm_sec) + "_" + QString::number(ltm->tm_mday) + "-" + QString::number(ltm->tm_mon) + "-" + QString::number(ltm->tm_year-100) + "_" + ".txt"; //construct logfile name using time and date to avoid matching logfile names

    if(!QDir("logging/").exists())
    {
        QDir().mkdir("logging/"); //create logging directory
    }

    QFile* file = new QFile("logging/" + logname);
    if (!file->open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    //start logfile construction
    QTextStream out(file);
    out << "-------------- DataInterpreter Logfile --------------" << endl;
    out << "Date and time of logging: " << dt;
    out << "Amount of channels in dataset: " << channelAmount << endl;
    if(!calibrated)
    {
        out << "Time not yet calibrated; Timestamps logged in amount of ticks." << endl;
    }
    out << "-----------------------------------------------------" << endl << endl;
    out << endl << "Start of log:";
    out << endl << endl;

    for(int i = 0; i < channelAmount; i++)
    {
        DataChannel* channel = TheF->theDS()->getDatachannel(i);
        out << "Datachannel ID: " << channel->getChannelID() << ", AD Converter " << channel->getConverter() << endl;
        out << "-----------------------------------------------------" << endl;
        for(unsigned int c = 0; c < channel->getData().size(); c++)
        {
            QString time;
            QString ticks_seconds;
            QString value;
            if(calibrated)
            {
                time = QString::number(std::floor((double(TheF->theDS()->getTimestamp(c) - TheF->theDS()->getTimestamp(0))/ticksPerSec) * 10.) / 10.); //get second timestamp, round to one decimal value
                if(std::fmod(time.toDouble(),1) == 0)
                {
                    time = time + ".0"; //add decimal .0 if time is integer value
                }
                ticks_seconds = "s";
            }
            else
            {
                time = QString::number(TheF->theDS()->getTimestamp(c) - TheF->theDS()->getTimestamp(0));
                ticks_seconds = " ticks";
            }

            value = QString::number(std::floor((verticalMinScale[i]+(double(double(channel->getValue(c))/MAX_VALUE)*double(verticalMaxScale[i]-verticalMinScale[i]))) * 10.) / 10.); //get value, round to one decimal value
            if(std::fmod(value.toDouble(),1) == 0)
            {
                value = value + ".0"; //add decimal .0 if value is integer
            }

            out << time + ticks_seconds + " - " + value << " " << dimensions[i] << endl;
        }
        out << "-----------------------------------------------------" << endl << endl << endl;
    }
}

void InputController::processCustomScript(QString type, int p1, int p2, int target)
{
    if((p1 != NULL && p2 != NULL) && (p1 == p2)) //script is invalid if both parameters are the same -> no data selected
    {
        emit sendScriptDisplayData(target, NULL, false);
    }
    else
    {
        if(p1 == NULL && p2 == NULL) //no parameters given
        {
            if(type == "avg")
            {
                DataChannel* tempChannel = TheF->theDS()->getDatachannel(target-1);
                double average = 0;
                for (unsigned int c = 0;c < tempChannel->getData().size();c++)
                {
                    average += (double(tempChannel->getValue(c)) / double(tempChannel->getData().size())); //calculate average
                }
                QString value = QString::number(std::floor((verticalMinScale[target-1]+(double(double(average)/MAX_VALUE)*double(verticalMaxScale[target-1]-verticalMinScale[target-1]))) * 10.) / 10.); //round to one decimal value
                if(std::fmod(value.toDouble(),1) == 0)
                {
                    value = value + ".0"; //add decimal .0 if value is integer
                }

                emit sendScriptDisplayData(target, value, true);
            }
            else if(type == "low")
            {
                DataChannel* tempChannel = TheF->theDS()->getDatachannel(target-1);
                int lowest = MAX_VALUE;
                for (unsigned int c = 0;c < tempChannel->getData().size();c++)
                {
                    if(tempChannel->getValue(c) < lowest)
                    {
                        lowest = tempChannel->getValue(c); //get lowest value
                    }
                }
                QString value = QString::number(std::floor((verticalMinScale[target-1]+(double(double(lowest)/MAX_VALUE)*double(verticalMaxScale[target-1]-verticalMinScale[target-1]))) * 10.) / 10.); //round to one decimal value
                if(std::fmod(value.toDouble(),1) == 0)
                {
                    value = value + ".0"; //add decimal .0 if value is integer
                }

                emit sendScriptDisplayData(target, value, true);
            }
            else if(type == "high")
            {
                DataChannel* tempChannel = TheF->theDS()->getDatachannel(target-1);
                int highest = 0;
                for (unsigned int c = 0;c < tempChannel->getData().size();c++)
                {
                    if(tempChannel->getValue(c) > highest)
                    {
                        highest = tempChannel->getValue(c); //get highest value
                    }
                }
                QString value = QString::number(std::floor((verticalMinScale[target-1]+(double(double(highest)/MAX_VALUE)*double(verticalMaxScale[target-1]-verticalMinScale[target-1]))) * 10.) / 10.); //round to one decimal value
                if(std::fmod(value.toDouble(),1) == 0)
                {
                    value = value + ".0"; //add decimal .0 if value is integer
                }

                emit sendScriptDisplayData(target, value, true);
            }
        }
        else //parameters given
        {
            DataChannel* tempChannel = TheF->theDS()->getDatachannel(target-1);
            int higherP, lowerP;

            if(p1 > p2) //define lower and higher param, to simplify caluculations below
            {
                higherP = p1;
                lowerP = p2;
            }
            else
            {
                higherP = p2;
                lowerP = p1;
            }

            if(type == "avg")
            {
                double average = 0;
                int counter = 0;
                for (unsigned int c = 0;c < tempChannel->getData().size();c++)
                {
                    if(((((double(TheF->theDS()->getTimestamps()[c])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) > lowerP)) && (((double(TheF->theDS()->getTimestamps()[c])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) < higherP)) //calibrated timestamp rule of three: check whether channel value timestamps falls between user selected time points
                    {
                        average += double(tempChannel->getValue(c));
                        counter++;
                    }
                }
                average = average / counter; //calculate average

                QString value = QString::number(std::floor((verticalMinScale[target-1]+(double(double(average)/MAX_VALUE)*double(verticalMaxScale[target-1]-verticalMinScale[target-1]))) * 10.) / 10.); //round to one decimal value
                if(std::fmod(value.toDouble(),1) == 0)
                {
                    value = value + ".0"; //add decimal .0 if value is integer
                }

                emit sendScriptDisplayData(target, value, true);
            }
            else if(type == "low")
            {
                int lowest = MAX_VALUE;
                for (unsigned int c = 0;c < tempChannel->getData().size();c++)
                {
                    if(((((double(TheF->theDS()->getTimestamps()[c])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) > lowerP)) && (((double(TheF->theDS()->getTimestamps()[c])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) < higherP)) //calibrated timestamp rule of three: check whether channel value timestamps falls between user selected time points
                    {
                        if(tempChannel->getValue(c) < lowest)
                        {
                            lowest = tempChannel->getValue(c); //get lowest value
                        }
                    }
                }

                QString value = QString::number(std::floor((verticalMinScale[target-1]+(double(double(lowest)/MAX_VALUE)*double(verticalMaxScale[target-1]-verticalMinScale[target-1]))) * 10.) / 10.); //round to one decimal value
                if(std::fmod(value.toDouble(),1) == 0)
                {
                    value = value + ".0"; //add decimal .0 if value is integer
                }

                emit sendScriptDisplayData(target, value, true);
            }
            else if(type == "high")
            {
                int highest = 0;
                for (unsigned int c = 0;c < tempChannel->getData().size();c++)
                {
                    if(((((double(TheF->theDS()->getTimestamps()[c])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) > lowerP)) && (((double(TheF->theDS()->getTimestamps()[c])-double(TheF->theDS()->getTimestamps()[0]))/ticksPerSec) < higherP)) //calibrated timestamp rule of three: check whether channel value timestamps falls between user selected time points
                    {
                        if(tempChannel->getValue(c) > highest)
                        {
                            highest = tempChannel->getValue(c);// get highest value
                        }
                    }
                }

                QString value = QString::number(std::floor((verticalMinScale[target-1]+(double(double(highest)/MAX_VALUE)*double(verticalMaxScale[target-1]-verticalMinScale[target-1]))) * 10.) / 10.); //round to one decimal value
                if(std::fmod(value.toDouble(),1) == 0)
                {
                    value = value + ".0"; //add decimal .0 if value is integer
                }

                emit sendScriptDisplayData(target, value, true);
            }
        }
    }
}

void InputController::setObserver(InputController::ISensorObserver* _obs)
{
    obs = _obs;
}

