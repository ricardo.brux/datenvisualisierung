#include "configurationwindow.h"
#include "../Factory/factory.h"

ConfigurationWindow::ConfigurationWindow(QWidget *parent)
    : QMainWindow(parent){}

ConfigurationWindow *ConfigurationWindow::getInstance()
{
    return this;
}

void ConfigurationWindow::init(Factory* F)
{
    TheF = F; //save factory pointer
    initGUI();
    this->show();
}

//UI initialisation.
void ConfigurationWindow::initGUI()
{
    setGeometry(200,80,250,120);
    setFocusPolicy(Qt::StrongFocus);
    setFixedSize(width(), height());

    QPalette pal = QPalette();
    pal.setColor(QPalette::Window, "#262a57");
    setAutoFillBackground(true);
    setPalette(pal);

    openPort = new QPushButton(this);
    closePort = new QPushButton(this);
    portList = new QComboBox(this);
    openPort->setGeometry(10,50,230,30);
    openPort->setText("Open Port");
    closePort->setGeometry(10,80,230,30);
    closePort->setText("Close Port");
    portList->setGeometry(10,10,230,30);
    closePort->setEnabled(false);

    this->setWindowTitle("DataInterpreter");
}

QComboBox *ConfigurationWindow::getPortList()
{
    return portList;
}

QPushButton *ConfigurationWindow::getClosePort()
{
    return closePort;
}

QPushButton *ConfigurationWindow::getOpenPort()
{
    return openPort;
}

void ConfigurationWindow::togglePortState(bool b)
{
    if(!b)
    {
        portList->setEnabled(true);
        openPort->setEnabled(true); 	//Used whenever no port is open. Avoids port-closing when none is open.
        closePort->setEnabled(false);
    }
    else
    {
        portList->setEnabled(false);
        openPort->setEnabled(false); 	//Used whenever a port is open. Avoids scenarios where user tries to open multiple ports at once.
        closePort->setEnabled(true);
    }
}

void ConfigurationWindow::disableWidgets()
{
    portList->setEnabled(false);
    openPort->setEnabled(false);
    closePort->setEnabled(false);
}

void ConfigurationWindow::displayInfo(QString message)
{
    QByteArray ba = message.toLocal8Bit();
    const char *c_str = ba.data(); 			//QMessageBox requires char* instead of QString, thus conversion.

    QMessageBox::information(this, tr("DataInterpreter"), tr(c_str)); //Display info message
}

void ConfigurationWindow::displayWarning(QString message)
{
    QByteArray ba = message.toLocal8Bit();
    const char *c_str = ba.data(); 			//QMessageBox requires char* instead of QString, thus conversion.

    QMessageBox::warning(this, tr("DataInterpreter"), tr(c_str)); //Display warning message
}

void ConfigurationWindow::closeEvent(QCloseEvent *event)
{
    if (event->spontaneous())
    {
        if(closePort->isEnabled())
        {
            emit logData();
            QMessageBox::information(this, tr("DataInterpreter"), tr("Logfile saved in application folder /logging.\nClosing application."));
        }
    }
    else
    {
        QWidget::closeEvent(event);
    }
}
