/*
 * ConfigurationWindow class:
 * Widget used to select port and open / close connection.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef CONFIGURATIONWINDOW_H
#define CONFIGURATIONWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QComboBox>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QCloseEvent>

class Factory; //Factory class declaration to avoid recursive includes. Factory is not static and thus the ConfigurationWindow has to save the Factory's pointer -> Factory* TheF.

class ConfigurationWindow : public QMainWindow
{
    Q_OBJECT

public:
    ConfigurationWindow(QWidget *parent = nullptr);
    ConfigurationWindow* getInstance(); //Return pointer to object
    void init(Factory* F); //Factory pointer passed in init() method
    void initGUI();
    QComboBox* getPortList();
    QPushButton* getClosePort();	//UI element getters.
    QPushButton* getOpenPort();
    void togglePortState(bool b); //disable / enable certain UI elements
    void disableWidgets(); //disable all UI elements - Used while uncertainty whether port is valid or not.
    void displayInfo(QString message); //Show pop-up message - Used to notify user of successful datalogging.
    void displayWarning(QString message); //Warning pop-up message - Used to alert user of invalid port.

private:
    Factory* TheF; //non-static factory pointer
    QPushButton* openPort;
    QPushButton* closePort;		//UI elements
    QComboBox* portList;
	
private slots:
    void closeEvent(QCloseEvent *event) override;

signals:
    void logData();
};

#endif // CONFIGURATIONWINDOW_H
