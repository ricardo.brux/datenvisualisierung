/*
 * DataWindow class:
 * Widget used to visualize data to give user options of manipulating said visualisation.
 *
 *
 * Written by Brux Ricardo
 * Version: 1.0
 * Date: 14.03.2022
 *
 */

#ifndef DATAWINDOW_H
#define DATAWINDOW_H

#include <QWidget>
#include <QPoint>
#include <QPushButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QLCDNumber>
#include <QLabel>
#include <QCheckBox>
#include <QPainter>
#include <cmath>

#include "../Controller/inputcontroller.h" //IC included for ISensorObserver

#define GRAPH_HEIGHT 170 //graph height in pixels
#define GRAPH_LENGTH 1000 //graph lenght in pixels
#define MAX_VALUE 931 //max 10bit value representing 3.0V (nucleo sends data between 0.0 to 3.0 volt)

class Factory; //Factory class declaration to avoid recursive includes. Factory is not static and thus the ConfigurationWindow has to save the Factory's pointer -> Factory* TheF.

class DataWindow : public QWidget, public InputController::ISensorObserver //inherits interface from InputController -> Observer pattern
{
    Q_OBJECT

public:
    DataWindow(QWidget *parent = nullptr);
    ~DataWindow();
    DataWindow* getInstance(); //Return pointer to object
    void init(Factory* F); //Factory pointer passed in init() method
    void initGUI();

    void transmitChannels(std::vector<DataChannel*> c); //view does not directly communicate with model, thus the controller provides necessary data through observer pattern
    void changeWindowSize(int w, int h); //scale window depending on amount of datachannels present
    void setChannelAmount(int n);
    void setCalibration(std::vector<int> timestamps, int ticksPerSec); //controller passes on calibration data - How many ticks equal one second? -> used for horizontal graph timescale
    void emitUpdateSignal(); //controller requesting y-axis scale and measurement unit data
    void resetCalibration(); //used when port is closed, resets calibration data
    void resetUI(); //used when port is closed, resets UI data

private:
    Factory* TheF; //non-static factory pointer
    int channelAmount;
    std::vector<DataChannel*> channels; //since view doesn't communicate with model, it has to store its own channel / timestamp data
    std::vector<int> timestamps;
    int ticksPerSec;
    bool calibrated;

    void scriptChanged(int c, QString s);

    // -- start of UI elements declaration --

    QLCDNumber* lcd1;
    QLCDNumber* lcd2;
    QLCDNumber* lcd3;
    QLCDNumber* lcd4;

    QLabel* namePlate1;
    QLabel* namePlate2;
    QLabel* namePlate3;
    QLabel* namePlate4;

    QLineEdit* dimension1;
    QLineEdit* dimension2;
    QLineEdit* dimension3;
    QLineEdit* dimension4;

    QDoubleSpinBox* minVertical1;
    QDoubleSpinBox* minVertical2;
    QDoubleSpinBox* minVertical3;
    QDoubleSpinBox* minVertical4;

    QDoubleSpinBox* maxVertical1;
    QDoubleSpinBox* maxVertical2;
    QDoubleSpinBox* maxVertical3;
    QDoubleSpinBox* maxVertical4;

    QDoubleSpinBox* minHorizontal1;
    QDoubleSpinBox* minHorizontal2;
    QDoubleSpinBox* minHorizontal3;
    QDoubleSpinBox* minHorizontal4;

    QDoubleSpinBox* maxHorizontal1;
    QDoubleSpinBox* maxHorizontal2;
    QDoubleSpinBox* maxHorizontal3;
    QDoubleSpinBox* maxHorizontal4;

    QLCDNumber* scriptinglcd1;
    QLCDNumber* scriptinglcd2;
    QLCDNumber* scriptinglcd3;
    QLCDNumber* scriptinglcd4;
    QLineEdit* scriptingInput1;
    QLineEdit* scriptingInput2;
    QLineEdit* scriptingInput3;
    QLineEdit* scriptingInput4;
    QCheckBox* scriptingCheck1;
    QCheckBox* scriptingCheck2;
    QCheckBox* scriptingCheck3;
    QCheckBox* scriptingCheck4;
    QLabel* showScriptingText1;
    QLabel* showScriptingText2;
    QLabel* showScriptingText3;
    QLabel* showScriptingText4;

    // -- end of UI elements declaration --

signals:
    void updateHorizontalScale(double, double, double, double, double, double, double, double); //update controller x-axis data
    void updateVerticalScale(double, double, double, double, double, double, double, double); //update controller y-axis data
    void updateDimensions(QString, QString, QString, QString); //update controller measurement unit data
    void sendCustomScript(QString, int, int, int); //send off custom script to be processed

private slots:
    void horizontalValuesChanged(double);
    void verticalValuesChanged(double);
    void dimensionsChanged(QString);
    void script1changed(QString);               //various UI onChanged slots
    void script2changed(QString);
    void script3changed(QString);
    void script4changed(QString);
    void updateScriptdisplay(int, QString, bool);

protected:
    void paintEvent(QPaintEvent *event);
};

#endif // DATAWINDOW_H
