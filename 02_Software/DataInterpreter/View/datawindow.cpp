#include "datawindow.h"
#include "../Factory/factory.h"

DataWindow::DataWindow(QWidget *parent)
    : QWidget(parent){}

DataWindow::~DataWindow(){}

DataWindow *DataWindow::getInstance()
{
    return this;
}

void DataWindow::init(Factory* F)
{
    TheF = F;
    initGUI();
}

void DataWindow::initGUI() //GUI initialization - mostly various definitions of UI objects
{
    setGeometry(460,50,1350,230);

    calibrated = false;

    QPalette pal = QPalette();
    pal.setColor(QPalette::Window, QColor(24,31,46,255));
    setAutoFillBackground(true);
    setPalette(pal);

    setWindowFlags(Qt::Window
        | Qt::WindowMinimizeButtonHint                      //Widget design - colors, fixed size, disabled maximisation button, etc.
        | Qt::WindowMaximizeButtonHint);

    setAttribute( Qt::WA_QuitOnClose, false );
    setFixedSize(width(), height());

    pal.setColor(pal.WindowText, QColor(255,255,255,255));
    setWindowTitle("DataInterpreter");

    // -- start of UI element definition --

    lcd1 = new QLCDNumber(this);
    lcd1->setGeometry(10,165,250,50);
    lcd1->setSegmentStyle(QLCDNumber::Flat);
    lcd1->setDigitCount(9);
    lcd1->setPalette(pal);
    lcd1->display(0.0);

    namePlate1 = new QLabel(this);
    namePlate1->setGeometry(62,130,150,50);
    namePlate1->setStyleSheet("QLabel{Color:white;}");

    dimension1 = new QLineEdit(this);
    dimension1->setGeometry(265,91,53,18);
    dimension1->setStyleSheet("background-color: #dddff0;");
    dimension1->setPlaceholderText("Unit");

    connect(dimension1,SIGNAL(textChanged(QString)),this,SLOT(dimensionsChanged(QString)));

    minVertical1 = new QDoubleSpinBox(this);
    minVertical1->setGeometry(273,167,45,18);
    maxVertical1 = new QDoubleSpinBox(this);
    maxVertical1->setGeometry(273,15,45,18);
    minVertical1->setStyleSheet("background-color: #d1d3e3;");
    maxVertical1->setStyleSheet("background-color: #d1d3e3;");
    minVertical1->setRange(-999999,999999);
    maxVertical1->setRange(-999999,999999);
    maxVertical1->setValue(100);

    connect(minVertical1,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));
    connect(maxVertical1,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));

    minHorizontal1 = new QDoubleSpinBox(this);
    minHorizontal1->setGeometry(335,202,45,18);
    maxHorizontal1 = new QDoubleSpinBox(this);
    maxHorizontal1->setGeometry(1190,202,45,18);
    minHorizontal1->setStyleSheet("background-color: #d1d3e3;");
    maxHorizontal1->setStyleSheet("background-color: #d1d3e3;");
    minHorizontal1->setRange(-999999,999999);
    maxHorizontal1->setRange(-999999,999999);
    maxHorizontal1->setValue(-5);

    connect(minHorizontal1,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));
    connect(maxHorizontal1,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));

    minHorizontal1->setEnabled(false);
    maxHorizontal1->setEnabled(false);

    scriptinglcd1 = new QLCDNumber(this);
    scriptinglcd1->setGeometry(10,15,250,50);
    scriptinglcd1->setSegmentStyle(QLCDNumber::Flat);
    scriptinglcd1->setDigitCount(9);
    scriptinglcd1->setPalette(pal);
    scriptinglcd1->display(0.0);
    scriptinglcd1->setVisible(false);

    scriptingInput1 = new QLineEdit(this);
    scriptingInput1->setGeometry(10,65,250,18);
    scriptingInput1->setStyleSheet("background-color: #dddff0;");
    scriptingInput1->setPlaceholderText("Scripting input");
    scriptingInput1->setVisible(false);

    scriptingCheck1 = new QCheckBox(this);
    scriptingCheck1->setGeometry(10,98,13,13);
    scriptingCheck1->setEnabled(false);

    showScriptingText1 = new QLabel(this);
    showScriptingText1->setGeometry(28,79,150,50);
    showScriptingText1->setStyleSheet("color: #999999;");
    showScriptingText1->setText("Show scripting options");

    connect(scriptingCheck1, SIGNAL(toggled(bool)), scriptinglcd1, SLOT(setVisible(bool)));
    connect(scriptingCheck1, SIGNAL(toggled(bool)), scriptingInput1, SLOT(setVisible(bool)));
    connect(scriptingInput1,SIGNAL(textChanged(QString)),this,SLOT(script1changed(QString)));

    //----------------------------------------------------------

    lcd2 = new QLCDNumber(this);
    lcd2->setGeometry(10,395,250,50);
    lcd2->setSegmentStyle(QLCDNumber::Flat);
    lcd2->setDigitCount(9);
    lcd2->setPalette(pal);
    lcd2->display(0.0);

    namePlate2 = new QLabel(this);
    namePlate2->setGeometry(62,360,150,50);
    namePlate2->setStyleSheet("QLabel{Color:white;}");

    dimension2 = new QLineEdit(this);
    dimension2->setGeometry(265,321,53,18);
    dimension2->setStyleSheet("background-color: #dddff0;");
    dimension2->setPlaceholderText("Unit");

    connect(dimension2,SIGNAL(textChanged(QString)),this,SLOT(dimensionsChanged(QString)));

    minVertical2 = new QDoubleSpinBox(this);
    minVertical2->setGeometry(273,397,45,18);
    maxVertical2 = new QDoubleSpinBox(this);
    maxVertical2->setGeometry(273,245,45,18);
    minVertical2->setStyleSheet("background-color: #d1d3e3;");
    maxVertical2->setStyleSheet("background-color: #d1d3e3;");
    minVertical2->setRange(-999999,999999);
    maxVertical2->setRange(-999999,999999);
    maxVertical2->setValue(100);

    connect(minVertical2,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));
    connect(maxVertical2,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));

    minHorizontal2 = new QDoubleSpinBox(this);
    minHorizontal2->setGeometry(335,432,45,18);
    maxHorizontal2 = new QDoubleSpinBox(this);
    maxHorizontal2->setGeometry(1190,432,45,18);
    minHorizontal2->setStyleSheet("background-color: #d1d3e3;");
    maxHorizontal2->setStyleSheet("background-color: #d1d3e3;");
    minHorizontal2->setRange(-999999,999999);
    maxHorizontal2->setRange(-999999,999999);
    maxHorizontal2->setValue(-5);

    connect(minHorizontal2,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));
    connect(maxHorizontal2,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));

    minHorizontal2->setEnabled(false);
    maxHorizontal2->setEnabled(false);

    scriptinglcd2 = new QLCDNumber(this);
    scriptinglcd2->setGeometry(10,245,250,50);
    scriptinglcd2->setSegmentStyle(QLCDNumber::Flat);
    scriptinglcd2->setDigitCount(9);
    scriptinglcd2->setPalette(pal);
    scriptinglcd2->display(0.0);
    scriptinglcd2->setVisible(false);

    scriptingInput2 = new QLineEdit(this);
    scriptingInput2->setGeometry(10,295,250,18);
    scriptingInput2->setStyleSheet("background-color: #dddff0;");
    scriptingInput2->setPlaceholderText("Scripting input");
    scriptingInput2->setVisible(false);

    scriptingCheck2 = new QCheckBox(this);
    scriptingCheck2->setGeometry(10,328,13,13);
    scriptingCheck2->setEnabled(false);

    showScriptingText2 = new QLabel(this);
    showScriptingText2->setGeometry(28,309,150,50);
    showScriptingText2->setStyleSheet("color: #999999;");
    showScriptingText2->setText("Show scripting options");

    connect(scriptingCheck2, SIGNAL(toggled(bool)), scriptinglcd2, SLOT(setVisible(bool)));
    connect(scriptingCheck2, SIGNAL(toggled(bool)), scriptingInput2, SLOT(setVisible(bool)));
    connect(scriptingInput2,SIGNAL(textChanged(QString)),this,SLOT(script2changed(QString)));

    //----------------------------------------------------------

    lcd3 = new QLCDNumber(this);
    lcd3->setGeometry(10,625,250,50);
    lcd3->setSegmentStyle(QLCDNumber::Flat);
    lcd3->setDigitCount(9);
    lcd3->setPalette(pal);
    lcd3->display(0.0);

    namePlate3 = new QLabel(this);
    namePlate3->setGeometry(62,590,150,50);
    namePlate3->setStyleSheet("QLabel{Color:white;}");

    dimension3 = new QLineEdit(this);
    dimension3->setGeometry(265,551,53,18);
    dimension3->setStyleSheet("background-color: #dddff0;");
    dimension3->setPlaceholderText("Unit");

    connect(dimension3,SIGNAL(textChanged(QString)),this,SLOT(dimensionsChanged(QString)));

    minVertical3 = new QDoubleSpinBox(this);
    minVertical3->setGeometry(273,627,45,18);
    maxVertical3 = new QDoubleSpinBox(this);
    maxVertical3->setGeometry(273,475,45,18);
    minVertical3->setStyleSheet("background-color: #d1d3e3;");
    maxVertical3->setStyleSheet("background-color: #d1d3e3;");
    minVertical3->setRange(-999999,999999);
    maxVertical3->setRange(-999999,999999);
    maxVertical3->setValue(100);

    connect(minVertical3,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));
    connect(maxVertical3,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));

    minHorizontal3 = new QDoubleSpinBox(this);
    minHorizontal3->setGeometry(335,662,45,18);
    maxHorizontal3 = new QDoubleSpinBox(this);
    maxHorizontal3->setGeometry(1190,662,45,18);
    minHorizontal3->setStyleSheet("background-color: #d1d3e3;");
    maxHorizontal3->setStyleSheet("background-color: #d1d3e3;");
    minHorizontal3->setRange(-999999,999999);
    maxHorizontal3->setRange(-999999,999999);
    maxHorizontal3->setValue(-5);

    connect(minHorizontal3,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));
    connect(maxHorizontal3,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));

    minHorizontal3->setEnabled(false);
    maxHorizontal3->setEnabled(false);

    scriptinglcd3 = new QLCDNumber(this);
    scriptinglcd3->setGeometry(10,475,250,50);
    scriptinglcd3->setSegmentStyle(QLCDNumber::Flat);
    scriptinglcd3->setDigitCount(9);
    scriptinglcd3->setPalette(pal);
    scriptinglcd3->display(0.0);
    scriptinglcd3->setVisible(false);

    scriptingInput3 = new QLineEdit(this);
    scriptingInput3->setGeometry(10,525,250,18);
    scriptingInput3->setStyleSheet("background-color: #dddff0;");
    scriptingInput3->setPlaceholderText("Scripting input");
    scriptingInput3->setVisible(false);

    scriptingCheck3 = new QCheckBox(this);
    scriptingCheck3->setGeometry(10,558,13,13);
    scriptingCheck3->setEnabled(false);

    showScriptingText3 = new QLabel(this);
    showScriptingText3->setGeometry(28,539,150,50);
    showScriptingText3->setStyleSheet("color: #999999;");
    showScriptingText3->setText("Show scripting options");

    connect(scriptingCheck3, SIGNAL(toggled(bool)), scriptinglcd3, SLOT(setVisible(bool)));
    connect(scriptingCheck3, SIGNAL(toggled(bool)), scriptingInput3, SLOT(setVisible(bool)));
    connect(scriptingInput3,SIGNAL(textChanged(QString)),this,SLOT(script3changed(QString)));

    //----------------------------------------------------------

    lcd4 = new QLCDNumber(this);
    lcd4->setGeometry(10,855,250,50);
    lcd4->setSegmentStyle(QLCDNumber::Flat);
    lcd4->setDigitCount(9);
    lcd4->setPalette(pal);
    lcd4->display(0.0);

    namePlate4 = new QLabel(this);
    namePlate4->setGeometry(62,820,150,50);
    namePlate4->setStyleSheet("QLabel{Color:white;}");

    dimension4 = new QLineEdit(this);
    dimension4->setGeometry(265,781,53,18);
    dimension4->setStyleSheet("background-color: #dddff0;");
    dimension4->setPlaceholderText("Unit");

    connect(dimension4,SIGNAL(textChanged(QString)),this,SLOT(dimensionsChanged(QString)));

    minVertical4 = new QDoubleSpinBox(this);
    minVertical4->setGeometry(273,857,45,18);
    maxVertical4 = new QDoubleSpinBox(this);
    maxVertical4->setGeometry(273,705,45,18);
    minVertical4->setStyleSheet("background-color: #d1d3e3;");
    maxVertical4->setStyleSheet("background-color: #d1d3e3;");
    minVertical4->setRange(-999999,999999);
    maxVertical4->setRange(-999999,999999);
    maxVertical4->setValue(100);

    connect(minVertical4,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));
    connect(maxVertical4,SIGNAL(valueChanged(double)),this,SLOT(verticalValuesChanged(double)));

    minHorizontal4 = new QDoubleSpinBox(this);
    minHorizontal4->setGeometry(335,892,45,18);
    maxHorizontal4 = new QDoubleSpinBox(this);
    maxHorizontal4->setGeometry(1190,892,45,18);
    minHorizontal4->setStyleSheet("background-color: #d1d3e3;");
    maxHorizontal4->setStyleSheet("background-color: #d1d3e3;");
    minHorizontal4->setRange(-999999,999999);
    maxHorizontal4->setRange(-999999,999999);
    maxHorizontal4->setValue(-5);

    connect(minHorizontal4,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));
    connect(maxHorizontal4,SIGNAL(valueChanged(double)),this,SLOT(horizontalValuesChanged(double)));

    minHorizontal4->setEnabled(false);
    maxHorizontal4->setEnabled(false);

    scriptinglcd4 = new QLCDNumber(this);
    scriptinglcd4->setGeometry(10,705,250,50);
    scriptinglcd4->setSegmentStyle(QLCDNumber::Flat);
    scriptinglcd4->setDigitCount(9);
    scriptinglcd4->setPalette(pal);
    scriptinglcd4->display(0.0);
    scriptinglcd4->setVisible(false);

    scriptingInput4 = new QLineEdit(this);
    scriptingInput4->setGeometry(10,755,250,18);
    scriptingInput4->setStyleSheet("background-color: #dddff0;");
    scriptingInput4->setPlaceholderText("Scripting input");
    scriptingInput4->setVisible(false);

    scriptingCheck4 = new QCheckBox(this);
    scriptingCheck4->setGeometry(10,788,13,13);
    scriptingCheck4->setEnabled(false);

    showScriptingText4 = new QLabel(this);
    showScriptingText4->setGeometry(28,769,150,50);
    showScriptingText4->setStyleSheet("color: #999999;");
    showScriptingText4->setText("Show scripting options");

    connect(scriptingCheck4, SIGNAL(toggled(bool)), scriptinglcd4, SLOT(setVisible(bool)));
    connect(scriptingCheck4, SIGNAL(toggled(bool)), scriptingInput4, SLOT(setVisible(bool)));
    connect(scriptingInput4,SIGNAL(textChanged(QString)),this,SLOT(script4changed(QString)));

    connect(TheF->theIC(),SIGNAL(sendScriptDisplayData(int, QString, bool)),this,SLOT(updateScriptdisplay(int, QString, bool)));
}

void DataWindow::transmitChannels(std::vector<DataChannel *> c) //Receive channels from controller. Output most recent value (relativ to user x-axis input) to LCDs.
{
    channels = c;

    if(channelAmount > 0) //Similar code repeated four times for a maximum of four channels. Due to liberal use of UI elements (with fixed names) in the calculations / statements below, optimising code through outsourced functions or for() loops wouldn't really simplify the code.
    {
        double tempDouble;
        if(!calibrated)
        { //Scale first value in channel to user y-axis input scale. Round resulting double to single decimal point.
            tempDouble = std::floor((minVertical1->value()+(double(double(channels[0]->getLastValue())/MAX_VALUE)*double(maxVertical1->value()-minVertical1->value()))) * 10.) / 10.;
        }
        else
        { //Due to calulation optimisation in controller, if calibrated, received channel vector is inverted -> Most recent value has index 0.
            tempDouble = std::floor((minVertical1->value()+(double(double(channels[0]->getFirstValue())/MAX_VALUE)*double(maxVertical1->value()-minVertical1->value()))) * 10.) / 10.;
        }

        if(std::fmod(tempDouble,1) == 0) //Check whether value has decimal point number -> If value is an integer value (for example 3 or 5), then no decimal point exist.
        {                                //In this case, add manual decimal .0. This ensures consistent application output, constant change in ouput format is jarring.
            QString tempString = QString::number(tempDouble) + ".0";
            lcd1->display(tempString);
        }
        else
        {
            lcd1->display(tempDouble);
        }

        //Display channel metadata
        namePlate1->setText("Channel ID: " + QString::number(channels[0]->getChannelID()) + ", AD Converter " + QString::number(channels[0]->getConverter()));
    }
    if(channelAmount > 1) //Repeat for remaining channels.
    {
        double tempDouble;
        if(!calibrated)
        {
            tempDouble = std::floor((minVertical2->value()+(double(double(channels[1]->getLastValue())/MAX_VALUE)*double(maxVertical2->value()-minVertical2->value()))) * 10.) / 10.;
        }
        else
        {
            tempDouble = std::floor((minVertical2->value()+(double(double(channels[1]->getFirstValue())/MAX_VALUE)*double(maxVertical2->value()-minVertical2->value()))) * 10.) / 10.;
        }

        if(std::fmod(tempDouble,1) == 0)
        {
            QString tempString = QString::number(tempDouble) + ".0";
            lcd2->display(tempString);
        }
        else
        {
            lcd2->display(tempDouble);
        }

        namePlate2->setText("Channel ID: " + QString::number(channels[1]->getChannelID()) + ", AD Converter " + QString::number(channels[1]->getConverter()));
    }
    if(channelAmount > 2)
    {
        double tempDouble;
        if(!calibrated)
        {
            tempDouble = std::floor((minVertical3->value()+(double(double(channels[2]->getLastValue())/MAX_VALUE)*double(maxVertical3->value()-minVertical3->value()))) * 10.) / 10.;
        }
        else
        {
            tempDouble = std::floor((minVertical3->value()+(double(double(channels[2]->getFirstValue())/MAX_VALUE)*double(maxVertical3->value()-minVertical3->value()))) * 10.) / 10.;
        }

        if(std::fmod(tempDouble,1) == 0)
        {
            QString tempString = QString::number(tempDouble) + ".0";
            lcd3->display(tempString);
        }
        else
        {
            lcd3->display(tempDouble);
        }

        namePlate3->setText("Channel ID: " + QString::number(channels[2]->getChannelID()) + ", AD Converter " + QString::number(channels[2]->getConverter()));
    }
    if(channelAmount > 3)
    {
        double tempDouble;
        if(!calibrated)
        {
            tempDouble = std::floor((minVertical4->value()+(double(double(channels[3]->getLastValue())/MAX_VALUE)*double(maxVertical4->value()-minVertical4->value()))) * 10.) / 10.;
        }
        else
        {
            tempDouble = std::floor((minVertical4->value()+(double(double(channels[3]->getFirstValue())/MAX_VALUE)*double(maxVertical4->value()-minVertical4->value()))) * 10.) / 10.;
        }

        if(std::fmod(tempDouble,1) == 0)
        {
            QString tempString = QString::number(tempDouble) + ".0";
            lcd4->display(tempString);
        }
        else
        {
            lcd4->display(tempDouble);
        }

        namePlate4->setText("Channel ID: " + QString::number(channels[3]->getChannelID()) + ", AD Converter " + QString::number(channels[3]->getConverter()));
    }

    update();
}

void DataWindow::changeWindowSize(int w, int h) //Update widget scaling
{
    setFixedSize(w, h);
    update();
}

void DataWindow::setChannelAmount(int n) //set fixed amount of channels
{
    channelAmount = n;
}

void DataWindow::setCalibration(std::vector<int> ts, int tps) //receive calibration data
{
    timestamps = ts;
    ticksPerSec = tps;

    calibrated = true;

    //If this method is called, then that means that calibration has ended -> Send x-axis values to controller to condition dataoutput.
    emit updateHorizontalScale(minHorizontal1->value(), minHorizontal2->value(), minHorizontal3->value(), minHorizontal4->value(), maxHorizontal1->value(), maxHorizontal2->value(), maxHorizontal3->value(), maxHorizontal4->value());

    //set x-axis value boundaries
    if(channelAmount > 0)
    {
        minHorizontal1->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec),((timestamps.back() - timestamps[0])/ticksPerSec)); //set left parameter boundary independent of right param.
        if(minHorizontal1->value() > 0)
        {
            maxHorizontal1->setRange(-minHorizontal1->value(),0); //set right Parameter boundary relative to left param. value
        }
        else
        {
            maxHorizontal1->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec)-minHorizontal1->value(),0); //different calculation depending if left param. is <0 or >0.
        }
    }
    if(channelAmount > 1) //repeat for other channels
    {
        minHorizontal2->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec),((timestamps.back() - timestamps[0])/ticksPerSec));
        if(minHorizontal2->value() > 0)
        {
            maxHorizontal2->setRange(-minHorizontal2->value(),0);
        }
        else
        {
            maxHorizontal2->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec)-minHorizontal2->value(),0);
        }
    }
    if(channelAmount > 2)
    {
        minHorizontal3->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec),((timestamps.back() - timestamps[0])/ticksPerSec));
        if(minHorizontal3->value() > 0)
        {
            maxHorizontal3->setRange(-minHorizontal3->value(),0);
        }
        else
        {
            maxHorizontal3->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec)-minHorizontal3->value(),0);
        }
    }
    if(channelAmount > 3)
    {
        minHorizontal4->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec),((timestamps.back() - timestamps[0])/ticksPerSec));
        if(minHorizontal4->value() > 0)
        {
            maxHorizontal4->setRange(-minHorizontal4->value(),0);
        }
        else
        {
            maxHorizontal4->setRange(-((timestamps.back() - timestamps[0])/ticksPerSec)-minHorizontal4->value(),0);
        }
    }

    minHorizontal1->setEnabled(true);
    maxHorizontal1->setEnabled(true);
    minHorizontal2->setEnabled(true);
    maxHorizontal2->setEnabled(true);
    minHorizontal3->setEnabled(true);       //enable params if they are disabled (disabled when calibration hasn't ended yet)
    maxHorizontal3->setEnabled(true);
    minHorizontal4->setEnabled(true);
    maxHorizontal4->setEnabled(true);

    scriptingCheck1->setEnabled(true);
    scriptingCheck2->setEnabled(true);
    scriptingCheck3->setEnabled(true);
    scriptingCheck4->setEnabled(true);
    showScriptingText1->setStyleSheet("color: #ffffff;");
    showScriptingText2->setStyleSheet("color: #ffffff;");
    showScriptingText3->setStyleSheet("color: #ffffff;");
    showScriptingText4->setStyleSheet("color: #ffffff;");
}

void DataWindow::emitUpdateSignal() //controller requesting y-axis and measurement unit data
{
    emit updateVerticalScale(minVertical1->value(), minVertical2->value(), minVertical3->value(), minVertical4->value(), maxVertical1->value(), maxVertical2->value(), maxVertical3->value(), maxVertical4->value());
    emit updateDimensions(dimension1->text(), dimension2->text(), dimension3->text(), dimension4->text());
}

void DataWindow::resetCalibration() //reset all calibration-related values
{
    calibrated = false;

    minHorizontal1->setEnabled(false);
    maxHorizontal1->setEnabled(false);
    minHorizontal2->setEnabled(false);
    maxHorizontal2->setEnabled(false);
    minHorizontal3->setEnabled(false);
    maxHorizontal3->setEnabled(false);
    minHorizontal4->setEnabled(false);
    maxHorizontal4->setEnabled(false);

    scriptingCheck1->setChecked(false);
    scriptingCheck2->setChecked(false);
    scriptingCheck3->setChecked(false);
    scriptingCheck4->setChecked(false);
    showScriptingText1->setStyleSheet("color: #999999;");
    showScriptingText2->setStyleSheet("color: #999999;");
    showScriptingText3->setStyleSheet("color: #999999;");
    showScriptingText4->setStyleSheet("color: #999999;");
    scriptingInput1->setText("");
    scriptingInput2->setText("");
    scriptingInput3->setText("");
    scriptingInput4->setText("");
    scriptingInput1->setStyleSheet("color: #000000;");
    scriptingInput2->setStyleSheet("color: #000000;");
    scriptingInput3->setStyleSheet("color: #000000;");
    scriptingInput4->setStyleSheet("color: #000000;");
    scriptinglcd1->display(0);
    scriptinglcd2->display(0);
    scriptinglcd3->display(0);
    scriptinglcd4->display(0);
}

void DataWindow::resetUI() //reset all generic UI elements
{
    minHorizontal1->setValue(0);
    maxHorizontal1->setValue(-5);
    minHorizontal2->setValue(0);
    maxHorizontal2->setValue(-5);
    minHorizontal3->setValue(0);
    maxHorizontal3->setValue(-5);
    minHorizontal4->setValue(0);
    maxHorizontal4->setValue(-5);

    minVertical1->setValue(0);
    maxVertical1->setValue(100);
    minVertical2->setValue(0);
    maxVertical2->setValue(100);
    minVertical3->setValue(0);
    maxVertical3->setValue(100);
    minVertical4->setValue(0);
    maxVertical4->setValue(100);

    dimension1->setText("");
    dimension2->setText("");
    dimension3->setText("");
    dimension4->setText("");
}

void DataWindow::horizontalValuesChanged(double)
{
    emit updateHorizontalScale(minHorizontal1->value(), minHorizontal2->value(), minHorizontal3->value(), minHorizontal4->value(), maxHorizontal1->value(), maxHorizontal2->value(), maxHorizontal3->value(), maxHorizontal4->value());
}

void DataWindow::verticalValuesChanged(double)
{
    emit updateVerticalScale(minVertical1->value(), minVertical2->value(), minVertical3->value(), minVertical4->value(), maxVertical1->value(), maxVertical2->value(), maxVertical3->value(), maxVertical4->value());
}

void DataWindow::dimensionsChanged(QString)
{
    emit updateDimensions(dimension1->text(), dimension2->text(), dimension3->text(), dimension4->text());
}

void DataWindow::script1changed(QString s)
{
    scriptChanged(1,s.toLower());
}

void DataWindow::script2changed(QString s)
{
    scriptChanged(2,s.toLower());
}

void DataWindow::script3changed(QString s)
{
    scriptChanged(3,s.toLower());
}

void DataWindow::script4changed(QString s)
{
    scriptChanged(4,s.toLower());
}

void DataWindow::scriptChanged(int c, QString s) //check script input
{
    if(s == "") //reset scriptdisplay if input is empty
    {
        if(c==1)
        {
            scriptingInput1->setStyleSheet("color: #000000;");
            scriptinglcd1->display(0);
        }
        else if(c==2)
        {
            scriptingInput2->setStyleSheet("color: #000000;");
            scriptinglcd2->display(0);
        }
        else if(c==3)
        {
            scriptingInput3->setStyleSheet("color: #000000;");
            scriptinglcd3->display(0);
        }
        else if(c==4)
        {
            scriptingInput4->setStyleSheet("color: #000000;");
            scriptinglcd4->display(0);
        }
    }
    else
    {
        QRegExp avgReg("average(\\(([0-9]+),([0-9]+)\\))?");
        QRegExp lowReg("lowest(\\(([0-9]+),([0-9]+)\\))?");     //define regex statements
        QRegExp highReg("highest(\\(([0-9]+),([0-9]+)\\))?");
        if(avgReg.exactMatch(s))
        {
            if(c==1)
            {
                scriptingInput1->setStyleSheet("color: #148c14;");
            }
            else if(c==2)
            {
                scriptingInput2->setStyleSheet("color: #148c14;");
            }
            else if(c==3)
            {
                scriptingInput3->setStyleSheet("color: #148c14;");
            }
            else if(c==4)
            {
                scriptingInput4->setStyleSheet("color: #148c14;");
            }

            if(avgReg.cap(1) == "")
            {
                emit sendCustomScript("avg", NULL, NULL, c);
            }
            else //if user defines parameters, pass them on
            {
                emit sendCustomScript("avg", avgReg.cap(2).toInt(), avgReg.cap(3).toInt(), c);
            }
        }
        else if(lowReg.exactMatch(s))
        {
            if(c==1)
            {
                scriptingInput1->setStyleSheet("color: #148c14;");
            }
            else if(c==2)
            {
                scriptingInput2->setStyleSheet("color: #148c14;");
            }
            else if(c==3)
            {
                scriptingInput3->setStyleSheet("color: #148c14;");
            }
            else if(c==4)
            {
                scriptingInput4->setStyleSheet("color: #148c14;");
            }

            if(lowReg.cap(1) == "")
            {
                emit sendCustomScript("low", NULL, NULL, c);
            }
            else //if user defines parameters, pass them on
            {
                emit sendCustomScript("low", lowReg.cap(2).toInt(), lowReg.cap(3).toInt(), c);
            }
        }
        else if(highReg.exactMatch(s))
        {
            if(c==1)
            {
                scriptingInput1->setStyleSheet("color: #148c14;");
            }
            else if(c==2)
            {
                scriptingInput2->setStyleSheet("color: #148c14;");
            }
            else if(c==3)
            {
                scriptingInput3->setStyleSheet("color: #148c14;");
            }
            else if(c==4)
            {
                scriptingInput4->setStyleSheet("color: #148c14;");
            }

            if(highReg.cap(1) == "")
            {
                emit sendCustomScript("high", NULL, NULL, c);
            }
            else //if user defines parameters, pass them on
            {
                emit sendCustomScript("high", highReg.cap(2).toInt(), highReg.cap(3).toInt(), c);
            }
        }
        else //if none of the statements succeed, paint input red
        {
            if(c==1)
            {
                scriptingInput1->setStyleSheet("color: #ff0000;");
                scriptinglcd1->display(0);
            }
            else if(c==2)
            {
                scriptingInput2->setStyleSheet("color: #ff0000;");
                scriptinglcd2->display(0);
            }
            else if(c==3)
            {
                scriptingInput3->setStyleSheet("color: #ff0000;");
                scriptinglcd3->display(0);
            }
            else if(c==4)
            {
                scriptingInput4->setStyleSheet("color: #ff0000;");
                scriptinglcd4->display(0);
            }
        }
    }
}

void DataWindow::updateScriptdisplay(int channel, QString value, bool valid) //receive processed script data
{
    if(channel == 1)
    {
        if(valid)
        {
            scriptinglcd1->display(value);
        }
        else
        {
            scriptinglcd1->display(0);
            scriptingInput1->setStyleSheet("color: #ff0000;");
        }
    }
    else if (channel == 2)
    {
        if(valid)
        {
            scriptinglcd2->display(value);
        }
        else
        {
            scriptinglcd2->display(0);
            scriptingInput2->setStyleSheet("color: #ff0000;");
        }
    }
    else if (channel == 3)
    {
        if(valid)
        {
            scriptinglcd3->display(value);
        }
        else
        {
            scriptinglcd3->display(0);
            scriptingInput3->setStyleSheet("color: #ff0000;");
        }
    }
    else if (channel == 4)
    {
        if(valid)
        {
            scriptinglcd4->display(value);
        }
        else
        {
            scriptinglcd4->display(0);
            scriptingInput4->setStyleSheet("color: #ff0000;");
        }
    }
}

void DataWindow::paintEvent(QPaintEvent *event)
{
    try
    {
        QPainter p(this);
        p.setPen(QColor(255,255,255,255));

        int vOffset;

        for (unsigned int c = 0; c < channelAmount; c++) //draw each channel graph
        {
            vOffset = c * 230; //vertical offset used by each consequent graph

            if(c > 0) //divider line - only drawn on subsequent graphs
            {
                p.setPen(QColor(120,100,200,200));
                p.drawLine(QPoint(0,vOffset),QPoint(1350,vOffset));
                p.setPen(QColor(180,180,255,255));
            }

            p.fillRect(QRect(335,15+vOffset,GRAPH_LENGTH,GRAPH_HEIGHT), QBrush(QColor(90,75,160,60), Qt::SolidPattern)); //rectangle to show graph area

            if(!calibrated) //Drawing data into the graphs. Each datapoint is connected with the previous and next to form a continuous line.
            {
                p.setPen(QColor(180,180,255,255));
                double tempValue = channels[c]->getValue(0);
                double tempSize = channels[c]->getData().size();
                QPoint previousPoint(335 + GRAPH_LENGTH,15+vOffset + GRAPH_HEIGHT - (tempValue/MAX_VALUE*GRAPH_HEIGHT)); //defining first point coordinates - rule of three to determine position on y-axis

                for (unsigned int i = 1;i <= channels[c]->getData().size() - 1; i++) //looping through all datapoints
                {
                    tempValue = channels[c]->getValue(i);
                    tempSize = channels[c]->getData().size();
                    QPoint tempPoint(335 + (GRAPH_LENGTH/(tempSize-1) * (tempSize - i - 1)),15+vOffset + GRAPH_HEIGHT - (tempValue/MAX_VALUE*GRAPH_HEIGHT)); //rule of three to determine y-axis and x-axis position

                    p.drawLine(previousPoint,tempPoint); //connect points

                    previousPoint = tempPoint;
                }
                p.setPen(QColor(255,255,255,255));
            }
            else //if calibrated, channel vectors are inverted; most recent value has index 0
            {
                p.setPen(QColor(180,180,255,255));
                QPoint previousPoint;

                int endElements=0;
                if(channels[c]->getCalibratedHorizontalMinIndex() == 1)     //necessary output range changes depending on left graph param.
                {
                    endElements=1;
                }

                for (unsigned int i = 0; i <= channels[c]->getData().size() - (2+endElements); i++)
                {
                    QPoint tempPoint(335 + (GRAPH_LENGTH/(double(timestamps[channels[c]->getCalibratedHorizontalMaxIndex() - 1])-double(timestamps[channels[c]->getCalibratedHorizontalMinIndex()])) * (timestamps[i + channels[c]->getCalibratedHorizontalMinIndex()] - timestamps[channels[c]->getCalibratedHorizontalMinIndex()])),15+vOffset + GRAPH_HEIGHT - (double(channels[c]->getValue(i))/MAX_VALUE*GRAPH_HEIGHT));

                    if(i != 0)
                    {
                        p.drawLine(previousPoint,tempPoint);
                    }

                    previousPoint = tempPoint;
                }
                QPoint finalpoint(335 + GRAPH_LENGTH,15+vOffset + GRAPH_HEIGHT - (double(channels[c]->getLastValue())/MAX_VALUE*GRAPH_HEIGHT)); //since vector is inverted, final point is defined instead of first

                p.drawLine(previousPoint,finalpoint);
                p.setPen(QColor(255,255,255,255));
            }

            p.drawLine(QPoint(335,15+vOffset),QPoint(335,15+vOffset + GRAPH_HEIGHT));
            p.drawLine(QPoint(335,15+vOffset + GRAPH_HEIGHT),QPoint(335 + GRAPH_LENGTH,15+vOffset + GRAPH_HEIGHT)); // draw graph y- and x-axis lines
        }
    }
    catch(Exception &e)
    {
        throw Exception(Exception::VISUALIZATIONERROR);
    }
}
