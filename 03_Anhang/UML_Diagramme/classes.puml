@startuml
'skinparam shadowing false
skinparam ArrowColor 2f3261
skinparam monochrome false
skinparam classAttributeIconSize 0
skinparam classBorderColor black
skinparam classBackgroundColor e9e3ff
'left to right direction
'hide circle
skinparam nodesep 160
skinparam ranksep 70
skinparam linetype ortho
skinparam classFontSize 15

class Factory
{
    +Factory()
    +init() : void
    +build() : void
    +start() : void
    {static} +theIC() : InputController*
    {static} +theDS() : DataSet*
    +theDW() : DataWindow*
    +theCW() : ConfigurationWindow*
--
	-TheDW : DataWindow*
	-TheCW : ConfigurationWindow*
}

class InputController {
    Q_OBJECT
--
    -InputController()
	{static} +getInstance() : InputController*
    +init(Factory*) : void
    +setObserver(ISensorObserver*) : void
	-writeCalibratedChannelData(int c) : DataChannel* 
    -reset() : void
--
    {static} -theIC : InputController
    -TheF : Factory*
	-serialPort: QSerialPort*
	-obs : ISensorObserver*
	-calibrationTimer : QTimer
	
	-serialPortOpen : bool
	-newConn : bool
	-ticksPerSec : int
	-calibrated : bool
	-calibrationMS : int
	-elapsedTime : int
	-channelAmount : int
	
	-horizontalMinScale : vector<double>
	-horizontalMaxScale : vector<double>
	
	-verticalMinScale : vector<double>
	-verticalMaxScale : vector<double>
	
	-dimensions : vector<QString>
--
    slots
	-onPortOpen() : void 
	-onPortClose() : void 
	-onSerialData() : void
	-calibrate() : void 
	-portInvalid() : void
	-updateHorizontalScale() : void
	-updateVerticalScale() : void
	-updateDimensionsScale() : void
	-createLog() : void
    -processCustomScript() : void
}

Interface ISensorObserver 
{
	+transmitChannels(vector<DataChannel*>) = 0 : virtual void
	+changeWindowSize(int, int) = 0 : virtual void
	+setChannelAmount(int) = 0 : virtual void
	+setCalibration(vector<int*>, int) = 0 : virtual void
	+emitUpdateSignal() = 0 : virtual void
	+resetCalibration() = 0 : virtual void
	+resetUI() = 0 : virtual void
--
}

class DataWindow {
    Q_OBJECT
--
    +DataWindow(QWidget*)
	+~DataWindow()
	+getInstance() : DataWindow*
    +init(Factory*) : void
    +initGUI() : void
	
	+transmitChannels(vector<DataChannel*>) : void
	+changeWindowSize(int, int) : void
	+setChannelAmount(int) = : void
	+setCalibration(vector<int*>, int) : void
	+emitUpdateSignal() : void
	+resetCalibration() : void
	+resetUI() : void
	
	-scriptChanged(int, QString) : void 
	
	+paintEvent(QPaintEvent*) : void 
--
    -TheF : Factory*
    -channelAmount : int
	-timestamps : vector<int>
	-channels : vector<DataChannel*>
	-ticksPerSec : int
	-calibrated : bool
	
	-lcd1 : QLCDNumber*
	-lcd2 : QLCDNumber*
	-lcd3 : QLCDNumber*
	-lcd4 : QLCDNumber*
	
	-namePlate1 : QLabel*
	-namePlate2 : QLabel*
	-namePlate3 : QLabel*
	-namePlate4 : QLabel*
	
	-dimension1 : QLineEdit*
	-dimension2 : QLineEdit*
	-dimension3 : QLineEdit*
	-dimension4 : QLineEdit*
	
	-minVertical1 : QDoubleSpinBox*
	-minVertical2 : QDoubleSpinBox*
	-minVertical3 : QDoubleSpinBox*
	-minVertical4 : QDoubleSpinBox*
	-maxVertical1 : QDoubleSpinBox*
	-maxVertical2 : QDoubleSpinBox*
	-maxVertical3 : QDoubleSpinBox*
	-maxVertical4 : QDoubleSpinBox*
	
	-minHorizontal1 : QDoubleSpinBox*
	-minHorizontal2 : QDoubleSpinBox*
	-minHorizontal3 : QDoubleSpinBox*
	-minHorizontal4 : QDoubleSpinBox*
	-maxHorizontal1 : QDoubleSpinBox*
	-maxHorizontal2 : QDoubleSpinBox*
	-maxHorizontal3 : QDoubleSpinBox*
	-maxHorizontal4 : QDoubleSpinBox*
	
	-scriptinglcd1 : QLCDNumber*
    -scriptinglcd2 : QLCDNumber*
    -scriptinglcd3 : QLCDNumber*
    -scriptinglcd4 : QLCDNumber*
    -scriptingInput1 : QLineEdit* 
    -scriptingInput2 : QLineEdit* 
    -scriptingInput3 : QLineEdit* 
    -scriptingInput4 : QLineEdit* 
    -scriptingCheck1 : QCheckBox* 
    -scriptingCheck2 : QCheckBox* 
    -scriptingCheck3 : QCheckBox* 
    -scriptingCheck4 : QCheckBox* 
    -showScriptingText1 : QLabel* 
    -showScriptingText2 : QLabel* 
    -showScriptingText3 : QLabel* 
    -showScriptingText4 : QLabel* 
--
    private slots
    horizontalValuesChanged(double) : void
    verticalValuesChanged(double) : void
    dimensionsChanged(QString) : void
    script1changed(QString) : void
    script2changed(QString) : void
    script3changed(QString) : void
    script4changed(QString) : void
    updateScriptdisplay(int, double) : void 
--
	signals
	updateHorizontalScale() : void
	updateVerticalScale() : void
	updateDimensions() : void
	sendCustomScript() : void
}

class DataSet
{
	Q_OBJECT
--
    -DataSet()
	{static} +getInstance() : DataSet*
	+getTimestamp(int) : int
	+addTimestamp(int) : void
	+getDatachannel(int) : DataChannel*
	+addDatachannel() : void
	+clearData() : void
	
	+getTimestamps() : vector<int> 
	+setTimestamps(vector<int>) : void
	
	+getChannels() : vector<DataChannel*> 
	+setChannels(vector<DataChannel*>) : void
--
    {static} -theDS : DataChannel
	-timestamps : vector<int>
	-channels : vector<DataChannel*>
}

class DataChannel
{
    +DataChannel()
	+addRecord() : void
	+replaceData(vector<int>) : void
	+getData() : vector<int>
	+getFirstValue() : int
	+getLastValue() : int
	+getValue(int) : int
	+getChannelID() : int
	+setChannelID(int) : void
	+getConverter() : int
	+setConverter(int) : void
	
	+getCalibratedHorizontalMinIndex() : int
	+setCalibratedHorizontalMinIndex(int) : void
	+getCalibratedHorizontalMaxIndex() : int
	+setCalibratedHorizontalMaxIndex(int) : void
--
	-channelID : int
	-converter : int
	-data : vector<int>
	
	-calibratedHorizontalMinIndex : int
	-calibratedHorizontalMaxIndex : int
}

class ConfigurationWindow
{
	+ConfigurationWindow(QWidget*)
	+getInstance() : ConfigurationWindow*
	+init(Factory*) : void
	+initGUI() : void
	+getPortList() : QComboBox*
	+getOpenPort() : QPushButton*
	+getClosePort() : QPushButton*
	+togglePortstate(bool) : void
	+disableWidgets() : void
	
	+displayInfo(QString) : void
	+displayWarning(QString) : void
--
	-TheF : Factory*
	-openPort : QPushButton*
	-closePort : QPushButton*
	-portList : QComboBox*
--
	private slots
	closeEvent(QCloseEvent*) : void
--
	signals
	logData() : void
}

class Exception
{
	+Exception(ERRORS)
	+what() : const char*
--
	-message : QString
}

enum ERRORS
{
	PARSINGERROR
	INVALIDTIMESTAMP
	VISUALIZATIONERROR
--
}

'Relations
'---------
	DataWindow --|> QObject
	ConfigurationWindow --|> QObject
	InputController --|> QObject
	DataSet --|> QObject
	InputController --> ISensorObserver : <<defines>>
	DataWindow --|> ISensorObserver
	Exception -left-> ERRORS : <<defines>>
	InputController .right.> Exception : <<uses>>
	DataWindow ..> Exception : <<uses>>
	
	Factory "-TheF" <--> "+theIC()" InputController
	Factory "-TheF" <--> "+theDS()" DataSet
	Factory "+theDW()" <--> "-TheF" DataWindow
	Factory "+theCW()" <-right-> "-TheF" ConfigurationWindow
	
	DataSet "-channels" o--> DataChannel
	DataWindow o--> "-channels" DataChannel
@enduml
