# DataInterpreter

Universal data visualization software developed by Brux Ricardo at the HES-SO Valais Wallis.

## Installation

The software can be installed through the provided Windows installer in /03_Anhang. 
Furthermore, the application is also usable on Linux and OSX, but the projects sourcecode found in /02_Software must be compiled through the Qt Creator first.
